package ru.tsc.ichaplygina.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.tsc.ichaplygina.taskmanager.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

    @Nullable
    User findByLogin(@NotNull String login);

}
